var http = require('http');
var httpProxy = require('http-proxy');

const options = {
    rejectUnauthorized:false
};

var proxy = httpProxy.createProxyServer(options); // See (†)
proxy.on('error', function(e) {
  console.log(e);
});

proxy.on('proxyRes', function(proxyRes, req, res) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'POST, GET, OPTIONS');
});


http.createServer(function(req, res) {
  proxy.web(req, res, {
    changeOrigin: true,
    protocol: 'https',
    target: 'https://yobit.net',
    host: 'yobit.net',
    rejectUnauthorized:false,
  });
}).listen(9000);
