import * as d3 from 'd3';

const drawChart = ({ height, offsetY }) => ({ xScale, yScale }) => points => (ctx) => {
  const line = d3.line()
    .x(d => xScale(d.timestamp))
    .y(d => yScale(d.close))
    // .curve(d3.curveStep)
    .context(ctx);

  const area = d3.area()
    .x(d => xScale(d.timestamp))
    .y1(d => yScale(d.close))
    .y0(() => height - offsetY)
    .context(ctx);

  ctx.lineWidth = 1;
  ctx.fillStyle = '#90AFDA';
  ctx.strokeStyle = '#285E77';
  area(points);
  ctx.fill();
  line(points);
  ctx.stroke();
};

export default drawChart;
