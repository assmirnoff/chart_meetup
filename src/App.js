import React, { Component } from 'react';
import Chart from './components/Chart';
import Pannel from './components/Pannel';

import chartStore from './store/ChartStore';

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Pannel chartStore={chartStore} />
        <Chart chartStore={chartStore} points={chartStore.points} />
      </div>
    );
  }
}

export default App;
