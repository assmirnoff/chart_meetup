import { observable, action, computed } from 'mobx';
import { currencyPairs, getData, read } from '../api/currency';

class ChartStore {
    @observable points = [];
    @observable result = 'ok';
    @observable pair = 'btc_rur';
    @observable limit = 200;
    @observable offset = 0;
    // @observable viewportOffset = 60;
    @observable servertime = 0;

    constructor() {
      this.api = currencyPairs();
    }

    @action async loadPoints() {
      const socket = await this.api;
      const initData = await getData({ pair: "EURCHF", "size": 1, uuid: "J8U8PEKDAIXQILF6H68" })(socket);

      this.points = initData.data.reverse();

      while (true) {
        const data = await read(socket)
        if (data.pair) {
          this.points = [...this.points, data];
          // this.points = this.points.reverse();
        } else if(data.servertime) {
          this.servertime = data.servertime;
        }
      }
    }

    @computed get pointsInViewport() {
      const { limit, servertime, offset } = this;
      const start = servertime - (limit + offset);
      const end = servertime - offset;
      return this.points
        .filter(point => (point.time >= start && point.time <= end))
        .map(point => ({ ...point, timestamp: point.time * 1000 }));
    }
}

export default new ChartStore();
