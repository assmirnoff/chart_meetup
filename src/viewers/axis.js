export const xAxis = xScale => settings => ctx => {
  const { height, offsetY } = settings;
  //console.log('xAxis-xScale-settings-ctx', ctx);
  const tickCount = 10;
  const tickSize = 6;
  const ticks = xScale.ticks(tickCount);
  const tickFormat = xScale.tickFormat();

  ctx.beginPath();
  // console.log('xAxis-tick-d', ticks);
  ticks.forEach(function (d) {
    ctx.moveTo(xScale(d), height - tickSize - offsetY);
    ctx.lineTo(xScale(d), height - offsetY);
  });
  ctx.strokeStyle = "black";
  ctx.stroke();

  ctx.textAlign = 'center';
  ctx.textBaseline = 'top';
  ticks.forEach(function (d) {
    ctx.fillText(tickFormat(d), xScale(d), height - offsetY - tickSize - 10);
  });
}

export const yAxis = yScale => settings => ctx => {
  const { offsetX } = settings;

  const tickCount = 20;
  const tickSize = 6;
  const ticks = yScale.ticks(tickCount);
  const tickFormat = yScale.tickFormat(tickCount);

  ctx.beginPath();
  ticks.forEach(function (d) {
    ctx.moveTo(offsetX, yScale(d));
    ctx.lineTo(offsetX + tickSize, yScale(d));
  });

  ctx.strokeStyle = 'black';
  ctx.stroke();
/*
  ctx.beginPath();
  ctx.moveTo(-tickSize, 0);
  ctx.lineTo(0, 0);
  ctx.lineTo(0.5, height);
  ctx.lineTo(-tickSize, height);
  ctx.strokeStyle = 'black';
  ctx.stroke();
*/
  ctx.textAlign = 'right';
  ctx.textBaseline = 'middle';
  ticks.forEach(function(d) {
    ctx.fillText(tickFormat(d), offsetX, yScale(d));
  });

  ctx.save();
  ctx.rotate(-Math.PI / 2);
  ctx.textAlign = 'right';
  ctx.textBaseline = 'top';
  ctx.font = 'bold 10px sans-serif';
  ctx.fillText('Price (US$)', -10, 10);
  ctx.restore();
};
