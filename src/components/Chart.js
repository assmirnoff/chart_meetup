import React, { Component } from 'react';
import { observer } from 'mobx-react';

import { draw } from '../viewers/chart';

@observer
export default class Chart extends Component {
  constructor(props) {
    super(props);
    this.settings = {
      width: 800,
      height: 800,
      offsetX: 100,
      offsetY: 100,
    };
    this.state = {
      mousePosition: { x: 0, y: 0 },
    };
  }

  componentWillMount() {
    this.props.chartStore.loadPoints();
  }

  componentWillReceiveProps() {
    console.log('wiill-receive-props');
  }

  _onMouseMove(e) {
    this.setState({ mousePosition: { x: e.nativeEvent.offsetX, y: e.nativeEvent.offsetY } });
  }

  render() {
    const { pointsInViewport, offset, limit, servertime } = this.props.chartStore;
    const { width, height } = this.settings;
    const { mousePosition } = this.state;
    return <div>
      <canvas ref={draw({ ...this.settings, offset, limit, servertime, mousePosition })(pointsInViewport)} width={width} height={height} onMouseMove={this._onMouseMove.bind(this)} />
    </div>
  }
}
