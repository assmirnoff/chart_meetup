import * as d3 from 'd3';
import drawChart from './drawChart';
import { xAxis, yAxis } from './axis';

const applyViewerToCanvas = ctx => viewer => {
  ctx.save();
  viewer(ctx)
  ctx.restore();
};

//const cross = ({ xScale, yScale }) => points => mousePosition => ctx => {
  //const { x, y } = mousePosition;
  //const mouseYValue = yScale.invert(y);
  //const closePoints = points.sort((a, b) => a.close - mouseYValue < b.close - mouseYValue);

  //console.log('chart-closePoints', mouseYValue, closePoints);
  //console.log('cross-xScale-invert', xScale(xScale.invert(x)), );
//}


const cross = ({ offsetX, offsetY, width, height }) => ({ xScale, yScale }) => points => mousePosition => ctx => {
  // ctx.moveTo(0, 0);
  const { x, y } = mousePosition;
  // console.log('cross-xScale-invert', xScale.invert(x), yScale.invert(y));

  const mouseYValue = yScale.invert(y);
  const mouseXValue = xScale.invert(x);

  const closePoints = [...points].sort((a, b) => {
    const aDiff = Math.abs(mouseXValue - a.timestamp);
    const bDiff = Math.abs(mouseXValue - b.timestamp);
    return aDiff - bDiff;
  });

  const closerPoint = closePoints[0];

  ctx.beginPath();
  if (closerPoint) {
    const closePointX = xScale(closerPoint.timestamp);
    const closePointY = yScale(closerPoint.close);
    ctx.moveTo(closePointX, offsetY);
    ctx.lineTo(closePointX, closePointY);
    ctx.lineTo(offsetX, closePointY);
    ctx.arc(closePointX, closePointY, 5, 0, 2 * Math.PI);
    ctx.stroke();

    ctx.fillText(`${closerPoint.close}`, closePointX, closePointY);
  } else {
    ctx.fillText(`${mouseYValue}`, x, y);
  }

  ctx.stroke();
};

export const draw = settings => points => canvas => {
  if (!canvas) {
    return;
  }

  const ctx = canvas.getContext('2d');
  const applyViewer = applyViewerToCanvas(ctx);

  const { height, width, offsetX, offsetY, mousePosition } = settings;
  const minClose = d3.min(points, point => point.close);
  const maxClose = d3.max(points, point => point.close);
  const yScale = d3.scaleLinear()
    .domain([minClose, maxClose])
    .range([offsetY, height - offsetY]);

  const minTime = d3.min(points, point => point.timestamp);
  const maxTime = d3.max(points, point => point.timestamp);

  // .domain([(servertime - (offset + limit)) * 1000, (servertime - offset) * 1000])
  const xScale = d3.scaleTime()
    .domain([minTime, maxTime])
    .range([offsetX, width - offsetX]);

  ctx.clearRect(0, 0, 800, 800);

  applyViewer(drawChart(settings)({ xScale, yScale })(points));
  applyViewer(cross(settings)({ xScale, yScale })(points)(mousePosition));
  applyViewer(xAxis(xScale)(settings));
  applyViewer(yAxis(yScale)(settings));
};
