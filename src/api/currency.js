export function currencyPairs() {
  return new Promise((resolve, reject) => {
    const socket = new WebSocket('wss://t-taratorkin.olymptrade.com/ws2');
    console.log('socket-', socket);
    socket.onclose = reject;
    socket.onopen = () => resolve(socket);
  });
}

export function getData(data) {
  return (socket) => new Promise((resolve, reject) => {
    console.log('getData-socket', socket);

    socket.send(JSON.stringify(data));
    socket.onerror = reject;
    socket.onmessage = e => {
      resolve(JSON.parse(e.data));
    }
  });
};

export function read(socket) {
  return new Promise((resolve, reject) => {
    // socket.send(JSON.stringify(data));
    socket.onerror = reject;
    socket.onmessage = e => {
      resolve(JSON.parse(e.data));
    }
  });
}
