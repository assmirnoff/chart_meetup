import React, { Component } from 'react';
import { observer } from 'mobx-react';

@observer
export default class Pannel extends Component {
  setCurrency(e) {
    const { chartStore } = this.props;
    const { value } = e.target;
    chartStore.pair = value;
  }

  setLimit(e) {
    const { chartStore } = this.props;
    const { value } = e.target;
    chartStore.limit = value;
  }

  setOffset(e) {
    const { chartStore } = this.props;
    const { value } = e.target;
    chartStore.offset = value;
  }

  render() {
    const { chartStore: { pair, limit, offset } } = this.props;
    return (
      <div>
        <input type="text" value={pair} placeholder="currency" onChange={this.setCurrency.bind(this)} />
        <label htmlFor="chart-offset">Offset</label>:
        <input id="chart-offset" type="number" value={offset} placeholder="offset" min={0} step={10} onChange={this.setOffset.bind(this)} />
        <label htmlFor="chart-limit">Limit</label>:
        <input id="chart-limit" type="number" value={limit} placeholder="limit" min={10} step={10} onChange={this.setLimit.bind(this)} />
      </div>
    );
  }
}
